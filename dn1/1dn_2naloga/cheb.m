function p = cheb(n)
  % returns n-th chebishev polynomial
  if n == 0, p = 1;,
  elseif n == 1, p = [1, 0];,
  else, p = 2 * [cheb(n-1) 0] - [0 0 cheb(n-2)];
  end
end
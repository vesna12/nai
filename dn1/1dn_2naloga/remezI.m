function polinom = remezI(f, n, a, b, eps)
    % f funkcija (pazi, da je zapisana z .*, .^ ipd.)
    % n je stopnja aproksimacijskega polinoma, n+2 je število delilnih točk v E
    % interval je [a, b]
    % eps je natančnost
    E = linspace(a, b, n+2); % vrstica
    while 1
       [A, B] = sistem(E, f);
       X = A\B;
       m = abs(X(1));
       p = fliplr(X(2:end)');
       [y, ry] = maksi(f, p, a, b, 5000);
       rry = f(y) - polyval(p, y);
       
%         E
%         f(E) - polyval(p, E)
%         y
%         ry
%         xx = linspace(a, b, 2000);
%         plot(xx, f(xx), xx, polyval(p, xx))
%         grid on
%         grid minor
%         legend('f', 'p')
%         pause
%         plot(xx, f(xx) - polyval(p, xx))
%         pause
         
       % zaustavitveni pogoj
       if abs(ry) - m < eps
           break
       end
       
       if y >= E(end)
           if predznak(rry, f(E(end)) - polyval(p, E(end))) == 1 
               % enaka predznaka, ven vrzem E(end)
               E(end) = y;
           else
               % razlicna predznaka, ven vrzem E(1)
               E = [E(2:end), y];
           end
       elseif y <= E(1)
           if predznak(rry, f(E(1)) - polyval(p, E(1))) == 1 
               % enaka predznaka, ven vrzem E(1)
               E(1) = y;
           else
               % razlicna predznaka, ven vrzem E(end)
               E = [y, E(1:(end-1))];
           end
       else
          k = 1;
          while y >= E(k)
              k = k + 1;              
          end
          % torej je y na [E(k-1), E(k)]
          if predznak(rry, f(E(k-1)) - polyval(p, E(k-1))) == 1
              % y in E(k-1) imata enak predznak, torej moram E(k-1) vreci ven
              E(k-1) = y;
          else
              % y in E(k) imata enak predznak, E(k) vrzem ven
              E(k) = y;
          end 
       end

    end
    xx = linspace(a, b, 2000);
    polinom = p
    plot(xx, f(xx), xx, polyval(p, xx))
    pause
    plot(xx, f(xx) - polyval(p, xx))
    pause
    
end

%  remezI(@(x)(exp(-x) .* sin(4*x)), 4, -1, 1, 10^(-10), 6)
function [y, ry] = maksi(f, koef, a, b, l)
    % funkcija naredi delitev intervala [a, b] na l tock in poisce, kje je
    % dosezen max |f - p*|, kjer je polinom p* podan s koef
    xx = linspace(a, b, l);
%    M = a;
%      vr = 0;
%      for i = 1:l
%          trenutna = abs(f(xx(i)) - polyval(koef, xx(i)));
%          if trenutna > M
%              vr = trenutna;
%              M = xx(i);
%          end
%      end
    
    [val, ind] = max(abs(f(xx) - polyval(koef, xx)));
    y = xx(ind);
    ry = val;
    plot(xx, f(xx) - polyval(koef, xx))
end
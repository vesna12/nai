function [A, B] = sistem(E, f)
    % funkcija sestavi matriko in vektor sistema za Remezev postopek
    % pazi, da je v f .^, .* ipd. !!!
    % ideja: [(-1).^(1:4)', fliplr(vander(F))]
    mm = length(E);
    neki = fliplr(vander(E));
    neki = neki(:, 1:(end-1));
    A = [(-1).^(2:(mm+1))', neki];
    B = f(E)'; % ker je E vrstica
end
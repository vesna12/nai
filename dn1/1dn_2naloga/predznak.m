function p = predznak(u, v)
    % vrne 1, ce sta predznaka enaka in 0 sicer
    if sign(u) == sign(v)
        p = 1;
    else
        p = 0;
    end
end
% resim nalogo 2

f = @(x)(exp(-x) .* sin(4*x));
n = 4;
a = -1;
b = 1;
eps = 10^(-10);
m = 6;

pp = remezI(f, n, a, b, eps)
razvojpp = razvoj(pp)
qq = pp - razvojpp(1)*cheb(length(pp)-1)
qq = qq(2:end);

x = a:0.001:b;
normafpp = max(abs(f(x) - polyval(pp, x)));
normafqq = max(abs(f(x) - polyval(qq, x)));
disp(sprintf('|f-p| = %f', normafpp));
disp(sprintf('|f-q| = %f', normafqq));

xx = a:0.01:b;
plot(xx, f(xx), xx, polyval(pp, xx), xx, polyval(qq, xx));
grid on
legend('f', 'remes', 'cheb')
pause



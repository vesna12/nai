f = inline('exp(x)')
n = 5
a = -1;
b = 1;

bf = bernsteinOperator(f, n, a, b);
% pozor, evaluirati ga moram v 1/(b-a) .* (x - a)

bd = cell(4, 1);
bd{1, 1} = bf;
% poracunam se odvode
for k = 1:3
    bd{k+1, 1} = 1/(b-a) .* polyder(bd{k, 1}); 
    % pozor, evaluirati ga moram v 1/(b-a) .* (x - a)
end

%  % NARISEM GRAF
%  % graf
%  xx = linspace(a, b, 1000); 
%  % te tocke so na [a, b], torej moram Bern. operatorje evaluirati v 1/(b-a) .* (x - a)
%  yy = f(xx);
%  
%  plot(xx, yy, 'r')
%  grid on
%  grid minor
%  %axis([a, b, -10, 10])
%  hold on
%  
%  barva = ['b', 'y', 'm', 'c'];
%  
%  xx;
%  yy;
%  xxPremaknjeni = 1/(b-a) .* (xx - a);
%  polyval(bf, xxPremaknjeni);
%  
%  plot(xx, polyval(bf, xxPremaknjeni), 'g')
%  
%  for l = 2:4
%      plot(xx, polyval(bd{l, 1}, xxPremaknjeni), barva(l))
%  end
%  legend('f', 'bf', 'bf1', 'bf2', 'bf3')
%  %pause
%  hold off


%  RACUNAM NAPAKO in sproti še ocenjujem \alpha_k (c del naloge)
xi = linspace(-1, 1, 201);
xiPremaknjeni = 1/(b-a) .* (xi - a);
fi = f(xi);

odgovor = zeros(3, 4);

for l = 1:4
    l
    m = 1;
    napaka = 1;
    prejsnja = 0;
    alpha = -1;
    while napaka >= 0.3
        bf = bernsteinOperator(f, m, a, b);
        bd = cell(l, 1);
        bd{1, 1} = bf;
        for k = 1:(l-1)
            bd{k+1, 1} = 1/(b-a) .* polyder(bd{k, 1}); 
        end
        bb = bd{l, 1};
        napaka = max(fi - polyval(bb, xiPremaknjeni));
        if prejsnja ~= 0 & m ~= 1
            alpha = log(napaka/prejsnja)/log(m/(m-1));
        end
        prejsnja = napaka;
        m = m+1;
    end
    odgovor(1, l) = l-1;
    odgovor(2, l) = m;
    odgovor(3, l) = alpha;
    odgovor
end

odgovor

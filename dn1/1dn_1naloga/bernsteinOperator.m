function bf = bernsteinOperator(f, n, a, b)
    % funckija vrne polinom Bernsteinov operator za funkcijo f na [a, b] (torej naredim še premik (po potrebi))
    
    q = (b-a);
    % ker je tu x iz [0, 1], zelim ga pa v [a, b], moram v f vstaviti (b-a)*x - a
    
    % za sestevanje polinomov
    leftpadz = @(p1,p2) [zeros(1,max(0,numel(p2) - numel(p1))),p1];
    vsota = @(p1, p2) [leftpadz(p1, p2) + leftpadz(p2, p1)];
    
    
    bp = bernsteinPolinomi(n);
    bf = conv(f(a), bp{n+1, 2});
    for i = 1:n
        bf = vsota(bf, conv(f(q .* (i/n) + a), bp{n+1, i+2}));
    end
    bf;
end
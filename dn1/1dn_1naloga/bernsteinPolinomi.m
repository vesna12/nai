function bp = bernsteinPolinomi(n) 
    % funckija zgenerira Bernsteinove polinome do reda n (s pomocjo rekurzije)
    bp = cell(n+1, n+2); % tabela katere elementi so karkoli
    % bp{n+1, i+2} = B^n_i
    % zacetni pogoj - B^n_-1 = 0, B^0_i = 0
    for j = 1:n+2
        for k = 1:n+1
            bp{k, j} = [0];
        end
    end
    % zacetni pogoj - B^0_0
    bp{1, 2} = [1];
    
    % za sestevanje polinomov
    leftpadz = @(p1,p2) [zeros(1,max(0,numel(p2) - numel(p1))),p1];
    vsota = @(p1, p2) [leftpadz(p1, p2) + leftpadz(p2, p1)];
    
    for k = 2:n+1
        for j = 2:k+1
            prvi = conv([-1 1], bp{k-1, j});
            drugi = conv([1 0], bp{k-1, j-1});
            bp{k, j} = vsota(prvi, drugi);          
        end
    end
    bp;
        
end
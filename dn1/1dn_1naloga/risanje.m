bp = bernsteinPolinomi(5);

% obicajni:
xx = linspace(0, 1, 100);

m = 5;
for i = 2:(m+1)
    plot(xx, polyval(bp{m, i}, xx))
    hold on
end
pause
hold off

% operator
f = @(x)(exp(x));
bo = bernsteinOperator(f, 20, 0, 1);
xx = linspace(0, 1, 100);
plot(xx, f(xx), xx, polyval(bo, xx))
legend('f', 'bernf')
pause

% reparametrizirani:
xx2 = linspace(-1, 1, 200);

m = 5;
for i = 2:(m+1)
    plot(xx2, polyval(bp{m, i}, 1/2 .* (xx2 + 1)))
    hold on
end
pause
hold off

% operator reparametriziran
f = @(x)(exp(x));
bo = bernsteinOperator(f, 20, -1, 1);
xx = linspace(-1, 1, 100);
xxPremaknjeni = 1/2 .* (xx - a);
plot(xx, f(xx), xx, polyval(bo, xxPremaknjeni))
legend('f', 'bernf')
pause


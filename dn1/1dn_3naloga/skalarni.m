function s = skalarni(f, g)
    % izracuna skalarni produkt med polinomoma f in g, kot v nalogi 3
    s = zeros(1, 11);
    for j = 1:11
        i = j-1;
        xi = i/5 - 1;
        s(j) = xi^2 * polyval(f,xi) * polyval(g,xi);
    end
    s = 1/5 .* s;
    s = sum(s);
end
%format longEng
format long
f = inline('x .* cos(x)');

% za sestevanje/odstevanje polinomov
leftpadz = @(p1,p2) [zeros(1,max(0,numel(p2) - numel(p1))),p1];
vsota = @(p1, p2) [leftpadz(p1, p2) + leftpadz(p2, p1)];
razlika = @(p1, p2) [leftpadz(p1, p2) - leftpadz(p2, p1)];

[A, Q] = triclenska(6)

% narisem Q-je
xx = linspace(-1, 1, 300);
plot(xx, polyval(Q{1, 2},xx))
grid on
hold on
for k = 1:6
    plot(xx, polyval(Q{1, k+2},xx))
end
pause
hold off

p = [0];
for k = 1:7
   i = k-1;
   sk = skalarniFn(Q{1, i+2}, f);
   p = vsota(p, sk .* Q{1, i+2});
end

p
xx = linspace(-1, 1, 300);
plot(xx, f(xx), 'r', xx, polyval(p, xx), 'b')
legend('f', 'p')
grid on

napaka = abs(f(0) - polyval(p, 0))
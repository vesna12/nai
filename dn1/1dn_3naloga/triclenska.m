function [A, Q] = triclenska(n)
    % funkcija zgenerira matriko A koeficientov alpha in beta
    % ter cell Q, katerega elementi so polinomi Q_n
    % deluje v skladu z triclensko rekurzivno formulo

    % za sestevanje/odstevanje polinomov
    leftpadz = @(p1,p2) [zeros(1,max(0,numel(p2) - numel(p1))),p1];
    vsota = @(p1, p2) [leftpadz(p1, p2) + leftpadz(p2, p1)];
    razlika = @(p1, p2) [leftpadz(p1, p2) - leftpadz(p2, p1)];
    
    A = zeros(n+1, 2);
    Q = cell(1, n+2); % zacnem z Q_-1
    Qp = cell(1, n+2); % zacnem z Qp_-1
    Q{1, 1} = [0];
    Qp{1, 1} = [0];
    Qp{1, 2} = [1];
    norma = skalarni(Qp{1, 2}, Qp{1, 2});
    A(1, 2) = sqrt(norma);
    Q{1, 2} = Qp{1, 2} / A(1, 2);
    A(1, 1) = skalarni(conv([1 0], Q{1, 2}), Q{1, 2});
    
    for k = 1:n % mogoce tukaj do n-1
        Qp{1, k+2} = razlika(conv([1 (-1)*A(k, 1)], Q{1, k+1}), A(k, 2)*Q{1,k});
        norma = skalarni(Qp{1,k+2}, Qp{1,k+2});
        A(k+1, 2) = sqrt(norma);
        Q{1,k+2} = Qp{1,k+2} / A(k+1, 2);
        A(k+1, 1) = skalarni(conv([1 0], Q{1,k+2}), Q{1,k+2});  
    end  
    
end
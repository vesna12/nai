% resim 3. nalogo z vaj
f = @(x) ((1-x) .* sin(x));
df = @(x) (-sin(x) + (1-x).* cos(x));
xx = [0 0 1/2 1/2 5/6 5/6 pi-1 pi-1 pi+1 pi+1 2*pi 2*pi];
ff = f(xx);
dff = df(xx);
tt = 0:0.001:2*pi;
fx = f(tt);

[D, b] = Newton(xx, ff, dff, tt);

plot(tt, fx, tt, b)
legend('f', 'intPol')

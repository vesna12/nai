f = @(x)(x .* sin(x));
df = @(x)(sin(x) + x .* cos(x));

tp = [pi/3 pi/3 2*pi/3 2*pi/3 4*pi/3 4*pi/3 5*pi/3 5*pi/3];
tq = [pi/5 pi/5 3*pi/5 3*pi/5 7*pi/5 7*pi/5 9*pi/5 9*pi/5];

ftp = f(tp);
ftq = f(tq);
dftp = df(tp);
dftq = df(tq);

jj = 0:(2*pi/100):(2*pi);

[D, pj] = Newton(tp, ftp, dftp, jj);
[D2, qj] = Newton(tq, ftq, dftq, jj);

plot(jj, f(jj), jj, pj, jj, qj)
pause

napakap = max(abs(f(jj) - pj))
napakaq = max(abs(f(jj) - qj))

function lpx = lagpoly(X, f, x)
% Opis:
%  lagpoly vrne vrednosti Lagrangeevega interpolacijskega polinoma.
%
% Definicija:
%  lpx = lagpoly(X,f,x)
% 
% Vhodni podatki:
%  X    seznam interpolacijskih tock,
%  f    seznam vrednosti funkcije v interpolacijskih tockah,
%  x   seznam vrednosti, v katerih racunamo vrednost Lagrangeevega
%  polinoma.
%
% Izhodni podatki:
%  lpx   seznam vrednosti Lagrangeevega polinoma, ki interpolira vrednosti
%  seznama f v interpolacijskih tockah X, v tockah iz seznama x.
    
    lpx = 0;
    n = length(X) - 1;
    for i = 0:n
        lpx= lpx + f(i+1) .* lagb(X, i, x);
    end
    lpx;
    
end
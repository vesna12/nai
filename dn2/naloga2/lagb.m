function lx = lagb(X, i, x)
% Opis:
%  lagb vrne vrednosti Lagrangeeneva baznega polinoma
%
% Definicija:
% lx = lagb(X, i, x)
%
% Vhodni podatki:
%  X   seznam interpolacijskih tock
%  i   indeks lagrangeevega baznega polinoma (indeksiranje od 0 dalje)
%  x   seznam tock, v katerih racunamo vrednost Lagrangeevega baznega
%  polinoma
% 
% Izhodni podatek:
% lx   seznam vrednosti i-tega Lagrangeevega baznega polinoma z
% interpolacijskimi tockami X v tockah iz seznama x
    
    lx = 1;
    for k = 0:(length(X)-1)
        if k ~= i
            lx = lx .* (x - X(k+1)) ./ (X(i+1) - X(k+1));
        end
    end
    lx;

end
function [D, b] = Newton(xx, ff, dff, tt)
    % 3. naloga z vaj
    % xx je seznam interpolacijskih tock z veckratnostmi
    % enako pri ff in dff!!
    % interpolacijski pol. je st. <= n, n+1 = length(xx)
    
    % funkcija vrne D tabelo deljenih diferenc in 
    % b vrednosti interpolacijskega polinoma v tockah tt
    m = length(xx);
    n = m - 1;
    
    % sestavim tabelo deljenih diferenc D (xx niso so notri)
    D = zeros(m, m);
    D(:, 1) = ff;

    % i vrstica, k stolpec
    for k = 2:m
       for i = 1:(m+1-k)
           stevec = D(i, k-1) - D(i+1, k-1);
           imenovalec = xx(i) - xx(i+k-1);
           if imenovalec == 0
               D(i,k) = dff(i)/1;
           else
               D(i,k) = stevec/imenovalec;
           end
       end
    end
    D;
    
    a = D(1, :);
    b = a(end);
    for i = (length(a)-1):-1:1
        b = a(i) + b .* (tt - xx(i));
    end
    b;

end
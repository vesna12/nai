f = @(x)(sin(x));
t = [0 pi/12 pi/6 pi/2];
ft = f(t);

pravilno = asin(1/3)
% odgovor dobim tako, da interpoliram sinus na točkah ft z vrednostmi t
% tako dobim ravno INVERZ :) :)

% z Newtonovo obliko
[D, p] = Newton(ft, t, [], [1/3]);
D
p
napaka = abs(p - asin(1/3))

% z Lagrangeevo obliko
p2 = lagpoly(ft, t, [1/3])
napaka2 = abs(p2 - asin(1/3))
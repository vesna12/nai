function vrednost = Linter(f, n, skala, tt)
    % vrne L_1(f) za n vozlov, kjer je skalarni produkt vsota v tockah skala. Nato vrne se vrednosti v tockah tt.
    % najboljsa resitev po metodi najmanjsih kvadratov je resitev 
    % G a = <f, H>, kjer G Grammova matrika

    matrika = zeros(n+1,n+1);
    for i = 0:n
        for k = 0:n
            matrika(i+1, k+1) = H(i, n, skala) * (H(k, n, skala))';
        end
    end

    fs = f(skala);
    vektor = zeros(n+1, 1);
    for k = 0:n
        vektor(k+1) = fs * (H(k, n, skala))';
    end
    
    
    % predoloceni sistem matrika * alpha = vektor
    matrika;
    vektor;
    alpha = matrika\vektor;
    
    h = zeros(length(tt), n+1);
    for i = 0:n
        h(:, i+1) = H(i, n, tt);
    end
    vrednost = h * alpha;
    vrednost = vrednost';
end
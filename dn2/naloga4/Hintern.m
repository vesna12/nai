function z = Hintern(n, f, tt)
    % funkcija vrne vrednosti interpolacijskega polinoma za f z vrednostmi
    % v n ekvidistantnih tockah
    % vrne vrednosti v tockah tt
    
    vv = f(0:(1/n):1);
    z = Hinter(vv, tt);
end
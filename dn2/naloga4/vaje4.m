% resuje nalogo 4 z vaj, 
tt = 0:0.01:1;
for i = 0:5
  plot(tt, H(i, 5, tt))
  hold on
end
legend('0', '1', '2', '3', '4', '5')
hold off

% primer
f = @(x)(sin(2.*pi.*x));
ft = f(tt);
xx = 0:(1/20):1;
fxx = f(xx);
zz = Hinter(fxx, tt);
plot(tt, ft, tt, zz)
pause

n = 1;
jj = 0:(1/200):1;
fj = f(jj);
while 1
    zj = Hintern(n, f, jj);
    napaka = max(abs(fj - zj));
    if napaka < 0.01
        break
    else
        n = n+1
    end
end
plot(jj, fj, jj, zj)
pause



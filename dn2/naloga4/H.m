function Ft = H(i, n, tt)
    % resuje nalogo 4 z vaj
    % vrne vresnosti H_i v tockah tt, pri ekvidistantni delitvi [0,1] na n
    % tock
    % i = 0, 1, ..., n
    
    g = @(x)((n.*x - i + 1).*(((i-1)/n <= x)&(x <= i/n)));
    h = @(x)((i + 1 - n.*x).*((i/n <= x)&(x <= (i+1)/n)));
    
    if i == 0
        F = h;
    elseif i == n
        F = g;
    else
        F = @(x)(g(x) + h(x) - (x == i/n));
    end
    Ft = F(tt);
end
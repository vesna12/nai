% c del 4. naloge
n = 5;
% v resnici je to resevanje predolocenega sistema
% matrika so vrednosti H_i-jev v ustreznih tockah (j/10)
% vektro na desni so pa vresnosti (j/10)
% resitev so a_i, ki minimizirajo
% ker je pogoj za minimum enako kot za predoloceni sistem!

f = @(x)(x); % ker iscem ustrezen minimum
skala = (0:(1/10):1);
delilne = 0:0.01:1;
zz = Linter(f, 5, skala, delilne);

%se narisem

plot(delilne, zz)
pause
% je to res vse pravilno??


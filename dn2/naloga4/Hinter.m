function z = Hinter(vv, tt)
    % funkcija vrne vrednosti interpolacijskega polinoma z vrednostmi vv
    % length(vv) = n+1
    % vrne vrednosti vv tockah tt
    n = length(vv) - 1;
    hh = cell(n+1, 1);
    for i = 0:n
        hh{i+1, 1} = H(i, n, tt);
    end
    
    z = 0;
    for i = 0:n
        z = z + vv(i+1) .* hh{i+1, 1};
    end
    z;
end
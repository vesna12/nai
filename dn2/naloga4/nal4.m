f = @(x)(cos(2 .* pi .*x) ./ (1 + x));
n = 5;
tt = 0:(1/100):1;
If = Hintern(n, f, tt);
skala = 0:(1/30):1;
Lf = Linter(f, n, skala, tt);;
plot(tt, f(tt), tt, If, tt, Lf)
%legend('f', 'If', 'Lf')
pause

% ocena napake za n = 5
MIf = max(abs(f(tt) - If))
MLf = max(abs(f(tt) - Lf))

nI = 1;
ft = f(tt);
while 1
    Ifn = Hintern(nI, f, tt);
    napaka = max(abs(ft - Ifn));
    if napaka < 0.01
        break
    else
        nI = nI+1;
    end
end

nL = 1;
while 1
    Lfn = Linter(f, nL, skala, tt);
    napaka = max(abs(ft - Lfn));
    if napaka < 0.01
        break
    else
        nL = nL+1;
    end
end

nI
nL
plot(tt, ft, tt, Ifn, tt, Lfn)
pause
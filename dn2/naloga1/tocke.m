function [tt, t1, t2] = tocke(n)
    % funkcija zgenerira tocke (cos(2 pi t_i), sin(2 pi t_i)), kjer t_i=i/n; t_i da v tt, cos v t1, sin v t2
    tt = zeros(1, n+1);
    t1 = zeros(1, n+1);
    t2 = zeros(1, n+1);
    for i = 1:(n+1)
        tt(i) = (i-1)/n;
        t1(i) = cos(2*pi*tt(i));
        t2(i) = sin(2*pi*tt(i));
    end
end
format long

% resevanje 1. naloge z vaj
f = @(x)(x.*cos(x) - (6+x).*sin(x));
col = hsv(6);
delilne = 0:(1/100):(2*pi);
fdel = f(delilne);
plot(delilne, fdel)
hold on;

for k = 1:5
    n = 3*k;
    % interpolacijske tocke
    t = 0:(2*pi/n):(2*pi);
    ft = f(t);
        
    fL = lagpoly(t, ft, delilne);
    plot(delilne, fL, 'color', col(k, :)); 
    grid on
    grid minor
    
    napaka = max(abs(fdel - fL))
end
legend('f', '1', '2', '3', '4', '5')
pause
hold off

function r = razmerje(xPol, yPol)
    % funkcija vrne razmerje, kot je definirano v navodilu naloge
    % podatka sta vektorja vrednosti

    vektor = sqrt(xPol.^2 + yPol.^2);
    r = max(vektor) / min(vektor);
    
end
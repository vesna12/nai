format long

% resevanje 1. naloge
razmerja = zeros(10, 2);
col = hsv(11);
figure;
hold on;
delilne = 0:(1/200):1;
for k = 1:10
    n = 3*k;
    % tocke a so (t1, t2)
    [tt, t1, t2] = tocke(n);
        
    xP = lagpoly(tt, t1, delilne);
    yP = lagpoly(tt, t2, delilne);
    plot(t1, t2, 'o', 'color', col(k, :)); 
    plot(xP, yP, '-', 'color', col(k, :));
    grid on
    grid minor
    r = razmerje(xP, yP)
    razmerja(k, :) = [k, r];
end
legend('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')
pause
hold off
razmerja